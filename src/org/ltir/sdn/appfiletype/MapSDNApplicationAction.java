/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.appfiletype;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.IOColorLines;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

@ActionID(
        category = "Build",
        id = "org.ltir.sdn.appfiletype.MapSDNApplicationAction"
)
@ActionRegistration(
        iconBase = "org/ltir/sdn/appfiletype/Search-Engine-Optimization-icon.png",
        displayName = "#CTL_MapSDNApplicationAction"
)
@ActionReferences({
    @ActionReference(path = "Toolbars/Build", position = 326),
    @ActionReference(path = "Loaders/text/app+xml/Actions", position = 800)
})
@Messages("CTL_MapSDNApplicationAction=Map")
public final class MapSDNApplicationAction implements ActionListener {

    private final AppDataObject context;
    private final FileObject appXML;
    private final FileObject deviceXML;
    private final FileObject projectFolder;
    private final FileObject tmpFolder;
    private Color green = new Color(39, 174, 35);

    public MapSDNApplicationAction(AppDataObject context) throws IOException {
        this.context = context;
        appXML = context.getPrimaryFile();
        projectFolder = appXML.getParent();
        File tmpTest = new File(projectFolder.getPath() + File.separator + "tmp");
        if (tmpTest.isDirectory()) {
            FileObject[] projectChilds = projectFolder.getChildren();
            FileObject tmpTestObj = null;
            for (FileObject projectChild : projectChilds) {
                if (projectChild.getName().equals("tmp")) {
                    tmpTestObj = projectChild;
                    break;
                }
            }
            tmpFolder = tmpTestObj;
        } else {
            tmpFolder = projectFolder.createFolder("tmp");
        }
        FileObject[] projectChilds = projectFolder.getChildren();
        FileObject devTest = null;
        for (FileObject projectChild : projectChilds) {
            if (projectChild.getName().equals("device")) {
                devTest = projectChild;
                break;
            }
        }
        deviceXML = devTest;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        // TODO use context
        final InputOutput io = IOProvider.getDefault().getIO("Output - " + projectFolder.getName(), false);
        io.setFocusTaken(true);
        io.getOut().println("_________________________");
        io.getOut().println();
        io.getOut().println();
        io.getOut().println();
        io.getOut().println();
        String command = "/opt/ltiral/Mapper_ODP_OF.jar ";
        command += deviceXML.getPath();
        command += " ";
        command += appXML.getPath();
        command += " ";
        command += tmpFolder.getPath() + File.separator + "crosstable.rcf";
        command += " ";
        command += tmpFolder.getPath() + File.separator + "lattice.xml";
        command += " ";
        command += tmpFolder.getPath() + File.separator + "mapping.xml";
        int exit_val = 0;
        try {
            IOColorLines.println(io, "Start mapping... ", green);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        final Process p;
        try {
            p = Runtime.getRuntime().exec(command);

            new Thread(new Runnable() {
                public void run() {
                    BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = null;

                    try {
                        while ((line = input.readLine()) != null) {
                            io.getOut().println(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            p.waitFor();
            exit_val = p.exitValue();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
        String sucess_msg = "SDN Application Successfully Mapped!";
        String mapping_err_msg = "Application unable to be mapped into device profile...";
        String unknown_err_msg = "Error : Unexpected Error";
        if (exit_val == 0) {
            try {
                IOColorLines.println(io, sucess_msg, green);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } else if (exit_val == 2) {
            io.getErr().println(mapping_err_msg);
        } else {
            io.getErr().println(unknown_err_msg);
        }
    }
}
