/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.appfiletype;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.swing.JList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/**
 *
 * @author ltir-workstation-1
 */
public class AppVisualElementContent {

    private NodeList tableNodes;
    public Document doc;

    public AppVisualElementContent(final AppDataObject dObj, final JList TablesJList, final JPanel panel, AppVisualElement visualElement) {
        panel.setLayout(new BorderLayout());
        PipelineGraphScene scene = new PipelineGraphScene(visualElement);
        refrech(scene, dObj, TablesJList);
        FileObject fObj = dObj.getPrimaryFile();
        scene.validate();
        JComponent view = scene.createView();
        panel.add(view, 0);
        fObj.addFileChangeListener(new FileChangeAdapter() {
            public void fileChanged(FileEvent fe, final PipelineGraphScene scene) {
                scene.clean();
                TablesJList.clearSelection();
                refrech(scene, dObj, TablesJList);
                scene.validate();
                JComponent view = scene.createView();
                panel.remove(0);
                panel.add(view, 0);
            }
        });
    }

    private void refrech(final PipelineGraphScene scene, AppDataObject dObj, JList TablesJList) {

        FileObject fObj = dObj.getPrimaryFile();
        DocumentBuilder dBuilder;
        try {
            dBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            doc = dBuilder.parse(new File(fObj.getPath()));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
        }

        tableNodes = doc.getElementsByTagName("Table");
        String[] tableNames = new String[tableNodes.getLength()];

        for (int i = 0; i < tableNodes.getLength(); i++) {
            Node tableNode = tableNodes.item(i);
            NodeList field_list = ((Element) tableNode).getElementsByTagName("Field");
            NodeList set_field_list = ((Element) tableNode).getElementsByTagName("SET_FIELD");
            NodeList set_meta_list = ((Element) tableNode).getElementsByTagName("SET_META");
            NodeList push_tag_list = ((Element) tableNode).getElementsByTagName("PUSH_TAG");
            NodeList pop_tag_list = ((Element) tableNode).getElementsByTagName("POP_TAG");
            NodeList goto_list = ((Element) tableNode).getElementsByTagName("GOTO");
            NodeList clear_list = ((Element) tableNode).getElementsByTagName("CLEAR_ACTIONS");
            NodeList output_list = ((Element) tableNode).getElementsByTagName("OUTPUT");
            NamedNodeMap tableAtributes = tableNode.getAttributes();
            try {
                String tableID = tableAtributes.getNamedItem("Id").getNodeValue();
                String tableName = tableAtributes.getNamedItem("Name").getNodeValue();
                String Search = tableAtributes.getNamedItem("Search").getNodeValue();
                String NbEntry = tableAtributes.getNamedItem("NbEntry").getNodeValue();
                String widgetID = tableID;
                TableWidget table = (TableWidget) scene.addNode(widgetID);
                table.setID(tableID);
                table.setTableName(tableName);
                table.addAtribute(table.createAtribute("- ID=" + tableID));
                table.addAtribute(table.createAtribute("- NbEntry=" + NbEntry));
                table.addAtribute(table.createAtribute("- Search=" + Search));
                for (int k = 0; k < field_list.getLength(); k++) {
                    Node field = field_list.item(k);
                    String field_name = field.getAttributes().getNamedItem("Name").getNodeValue();
                    String field_id = field.getAttributes().getNamedItem("Id").getNodeValue();
                    table.addField(table.createField("f" + field_id + ": " + field_name));
                }
                int action_id = 0;
                for (int k = 0; k < set_field_list.getLength(); k++) {
                    Node set_field = set_field_list.item(k);
                    String set_field_name = set_field.getAttributes().getNamedItem("Field").getNodeValue();
                    table.addAction(table.createAction("a" + action_id + ": set_field " + set_field_name));
                    action_id++;
                }
                for (int k = 0; k < set_meta_list.getLength(); k++) {
                    Node set_meta = set_meta_list.item(k);
                    String set_meta_name = set_meta.getAttributes().getNamedItem("Meta").getNodeValue();
                    table.addAction(table.createAction("a" + action_id + ": set_meta " + set_meta_name));
                    action_id++;
                }
                for (int k = 0; k < push_tag_list.getLength(); k++) {
                    Node push_tag = push_tag_list.item(k);
                    String push_tag_name = push_tag.getAttributes().getNamedItem("Tag").getNodeValue();
                    table.addAction(table.createAction("a" + action_id + ": push_tag " + push_tag_name));
                    action_id++;
                }
                for (int k = 0; k < pop_tag_list.getLength(); k++) {
                    Node pop_tag = pop_tag_list.item(k);
                    String pop_tag_name = pop_tag.getAttributes().getNamedItem("Tag").getNodeValue();
                    table.addAction(table.createAction("a" + action_id + ": pop_tag " + pop_tag_name));
                    action_id++;
                }
                for (int k = 0; k < goto_list.getLength(); k++) {
                    Node gotoNode = goto_list.item(k);
                    String gotoID = gotoNode.getAttributes().getNamedItem("Table").getNodeValue();
                    table.addAction(table.createAction("a" + action_id + ": goto " + gotoID));
                    action_id++;
                }
                for (int k = 0; k < clear_list.getLength(); k++) {
                    Node clear = clear_list.item(k);
                    table.addAction(table.createAction("a" + action_id + ": clear_action"));
                    action_id++;
                }
                for (int k = 0; k < output_list.getLength(); k++) {
                    Node output = output_list.item(k);
                    String port = output.getAttributes().getNamedItem("Port").getNodeValue();
                    table.addAction(table.createAction("a" + action_id + ": output " + port));
                    action_id++;
                }
                String tableSearch = tableAtributes.getNamedItem("Search").getNodeValue();
                String tableSize = tableAtributes.getNamedItem("NbEntry").getNodeValue();
                tableNames[i] = "Table[Id='" + tableID + "', Name='" + tableName + "', Search='" + tableSearch + "', NbEntry='" + tableSize + "']";
            } catch (NullPointerException e) {

            }
        }
        for (int i = 0; i < tableNodes.getLength(); i++) {
            Node tableNode = tableNodes.item(i);
            NamedNodeMap tableAtributes = tableNode.getAttributes();
            try {
                String tableID = tableAtributes.getNamedItem("Id").getNodeValue();
                String tableName = tableAtributes.getNamedItem("Name").getNodeValue();
                String widgetID = tableID;
                NodeList tableChilds = tableNode.getChildNodes();
                for (int k = 0; k < tableChilds.getLength(); k++) {
                    Node tableChild = tableChilds.item(k);
                    String Nodetag = tableChild.getNodeName();
                    if (Nodetag.equals("Action")) {
                        NodeList Actions = tableChild.getChildNodes();
                        for (int j = 0; j < Actions.getLength(); j++) {
                            Node Action = Actions.item(j);
                            if (Action.getNodeName().equals("GOTO")) {
                                NamedNodeMap actionAttrs = Action.getAttributes();
                                String ID = actionAttrs.getNamedItem("Table").getNodeValue();
                                String edge = "goto" + tableID + " " + ID;
                                try {
                                    scene.addEdge(edge);
                                    scene.setEdgeSource(edge, widgetID);
                                    scene.setEdgeTarget(edge, ID);
                                } catch (AssertionError err) {
                                    scene.removeEdge(edge);
                                }
                            }
                        }
                    }
                }
            } catch (NullPointerException e) {

            }
        }
        TablesJList.setListData(tableNames);
    }
}
