/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.appfiletype;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ListModel;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.EditProvider;
import org.netbeans.api.visual.action.PopupMenuProvider;
import org.netbeans.api.visual.action.TwoStateHoverProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.border.Border;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.graph.layout.GraphLayout;
import org.netbeans.api.visual.graph.layout.GraphLayoutFactory;
import org.netbeans.api.visual.graph.layout.GraphLayoutListener;
import org.netbeans.api.visual.graph.layout.GraphLayoutSupport;
import org.netbeans.api.visual.graph.layout.GridGraphLayout;
import org.netbeans.api.visual.graph.layout.TreeGraphLayout;
import org.netbeans.api.visual.graph.layout.UniversalGraph;
import org.netbeans.api.visual.layout.Layout;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.layout.SceneLayout;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Widget;

/**
 *
 * @author lined
 */
class PipelineGraphScene extends GraphScene.StringGraph {

    private static final Border BORDER_4 = BorderFactory.createLineBorder(4);

    private final LayerWidget mainLayer;
    private final LayerWidget connectionLayer;

    private WidgetAction mouseHoverAction = ActionFactory.createHoverAction(new MyHoverProvider());
    private final WidgetAction moveAction = ActionFactory.createMoveAction();
    private WidgetAction popupMenuAction = ActionFactory.createPopupMenuAction(new MyPopupMenuProvider());
    private WidgetAction popupMenuMainAction = ActionFactory.createPopupMenuAction(new MainPopupMenuProvider());
    static AppVisualElement visualElement;

    public PipelineGraphScene(final AppVisualElement visualElement) {
        this.visualElement = visualElement;
        mainLayer = new LayerWidget(this);
        getActions().addAction(popupMenuMainAction);
        //mainLayer.setLayout(LAYOUT_1);
        addChild(mainLayer);

        connectionLayer = new LayerWidget(this);
        addChild(connectionLayer);

        getActions().addAction(mouseHoverAction);

        TreeGraphLayout<String, String> graphLayout = new TreeGraphLayout<String, String>(this, 100, -100, 50, 50, false);

        SceneLayout sceneGraphLayout = LayoutFactory.createSceneGraphLayout(this, graphLayout);
        sceneGraphLayout.invokeLayout();
    }

    private static class MyHoverProvider implements TwoStateHoverProvider {

        @Override
        public void unsetHovering(Widget widget) {
            widget.setBackground(Color.WHITE);
        }

        @Override
        public void setHovering(Widget widget) {
            widget.setBackground(Color.CYAN);
        }

    }

    public void clean() {
        mainLayer.removeChildren();
    }

    public void setLayerLayout(Layout layout) {
        mainLayer.setLayout(layout);
    }

    @Override
    protected Widget attachNodeWidget(String node) {
        TableWidget widget = new TableWidget(this);
        widget.getActions().addAction(moveAction);
        widget.getActions().addAction(mouseHoverAction);
        widget.getActions().addAction(popupMenuAction);
        mainLayer.addChild(widget);
        return widget;
    }

    @Override
    protected Widget attachEdgeWidget(String edge) {
        ConnectionWidget connection = new ConnectionWidget(this);
        connection.setTargetAnchorShape(AnchorShape.TRIANGLE_FILLED);
        connectionLayer.addChild(connection);
        return connection;
    }

    @Override
    protected void attachEdgeSourceAnchor(String edge, String oldSourceNode, String sourceNode) {
        Widget w = sourceNode != null ? findWidget(sourceNode) : null;
        ((ConnectionWidget) findWidget(edge)).setSourceAnchor(AnchorFactory.createRectangularAnchor(w));
    }

    @Override
    protected void attachEdgeTargetAnchor(String edge, String oldTargetNode, String targetNode) {
        Widget w = targetNode != null ? findWidget(targetNode) : null;
        ((ConnectionWidget) findWidget(edge)).setTargetAnchor(AnchorFactory.createRectangularAnchor(w));
    }

    public LayerWidget getMainLayer() {
        return mainLayer;
    }

    public LayerWidget getConnectionLayer() {
        return connectionLayer;
    }

    private static class MyPopupMenuProvider implements PopupMenuProvider {

        @Override
        public JPopupMenu getPopupMenu(Widget widget, Point localLocation) {
            JPopupMenu popupMenu = new JPopupMenu();
            TableWidget table = (TableWidget) widget;
            String ID = table.getID();
            JList tables = visualElement.getTablesJList();
            ListModel tables_model = tables.getModel();
            for(int i=0; i<tables_model.getSize(); i++){
                String table_model = tables_model.getElementAt(i).toString();
                if(table_model.contains("Id='" + ID + "'")){
                    tables.setSelectedIndex(i);
                    break;
                }
            }
            JMenuItem edit = new JMenuItem("Edit " + table.getTableName());
            edit.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    visualElement.publicEditJButtonActionPerformed();
                }
            });
            JMenuItem remove = new JMenuItem("Remove " + table.getTableName());
            remove.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    visualElement.publicRemoveJButtonActionPerformed();
                }
            });
            popupMenu.add(edit);
            popupMenu.add(remove);
            return popupMenu;
        }

    }

    private static class MainPopupMenuProvider implements PopupMenuProvider {

        @Override
        public JPopupMenu getPopupMenu(Widget widget, Point point) {
            JPopupMenu popupMenu = new JPopupMenu();
            JMenuItem add = new JMenuItem("Add New Table");
            add.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    visualElement.publicAddJButtonActionPerformed();
                }
            });
            popupMenu.add(add);
            return popupMenu;
        }

        
    }

}
