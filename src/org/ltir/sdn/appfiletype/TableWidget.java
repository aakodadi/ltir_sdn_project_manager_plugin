/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.appfiletype;

import java.awt.Font;
import java.awt.Image;
import org.netbeans.api.visual.border.Border;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.ImageWidget;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.SeparatorWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Utilities;

/**
 *
 * @author lined
 */
public class TableWidget extends Widget {
    private static final Image IMAGE_TABLE = Utilities.loadImage ("resources/table.gif"); // NOI18N
    private static final Image IMAGE_FIELD = Utilities.loadImage ("resources/field.gif"); // NOI18N
    private static final Image IMAGE_ACTION = Utilities.loadImage ("resources/action.gif"); // NOI18N
    
    private final LabelWidget tableName;
    private final Widget atributes;
    private final Widget actions;
    private final Widget fields;
    private static final Border BORDER_4 = BorderFactory.createEmptyBorder (4);
    private String name;
    private String ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
    public TableWidget (Scene scene) {
        super (scene);
        setLayout (LayoutFactory.createVerticalFlowLayout ());
        setBorder (BorderFactory.createLineBorder ());
        setOpaque (true);
        setCheckClipping (true);

        Widget classWidget = new Widget (scene);
        classWidget.setLayout (LayoutFactory.createHorizontalFlowLayout ());
        classWidget.setBorder (BORDER_4);

        ImageWidget classImage = new ImageWidget (scene);
        classImage.setImage (IMAGE_TABLE);
        classWidget.addChild (classImage);

        tableName = new LabelWidget (scene);
        tableName.setFont (scene.getDefaultFont ().deriveFont (Font.BOLD));
        classWidget.addChild (tableName);
        addChild (classWidget);

        addChild (new SeparatorWidget (scene, SeparatorWidget.Orientation.HORIZONTAL));
        
        atributes = new Widget (scene);
        atributes.setLayout (LayoutFactory.createVerticalFlowLayout ());
        atributes.setOpaque (false);
        atributes.setBorder (BORDER_4);
        addChild (atributes);

        addChild (new SeparatorWidget (scene, SeparatorWidget.Orientation.HORIZONTAL));

        fields = new Widget (scene);
        fields.setLayout (LayoutFactory.createVerticalFlowLayout ());
        fields.setOpaque (false);
        fields.setBorder (BORDER_4);
        addChild (fields);

        addChild (new SeparatorWidget (scene, SeparatorWidget.Orientation.HORIZONTAL));

        actions = new Widget (scene);
        actions.setLayout (LayoutFactory.createVerticalFlowLayout ());
        actions.setOpaque (false);
        actions.setBorder (BORDER_4);
        addChild (actions);
    }

    public String getTableName () {
        return name;
    }

    public void setTableName (String name) {
        this.tableName.setLabel (" " + name);
        this.name = name;
    }

    public Widget createField (String field) {
        Scene scene = getScene ();
        Widget widget = new Widget (scene);
        widget.setLayout (LayoutFactory.createHorizontalFlowLayout ());

        ImageWidget imageWidget = new ImageWidget (scene);
        imageWidget.setImage (IMAGE_FIELD);
        widget.addChild (imageWidget);

        LabelWidget labelWidget = new LabelWidget (scene);
        labelWidget.setLabel (field);
        widget.addChild (labelWidget);

         return widget;
    }

    public Widget createAction (String action) {
        Scene scene = getScene ();
        Widget widget = new Widget (scene);
        widget.setLayout (LayoutFactory.createHorizontalFlowLayout ());

        ImageWidget imageWidget = new ImageWidget (scene);
        imageWidget.setImage (IMAGE_ACTION);
        widget.addChild (imageWidget);

        LabelWidget labelWidget = new LabelWidget (scene);
        labelWidget.setLabel (action);
        widget.addChild (labelWidget);

        return widget;
    }

    public Widget createAtribute (String atribute) {
        Scene scene = getScene ();
        Widget widget = new Widget (scene);
        widget.setLayout (LayoutFactory.createHorizontalFlowLayout ());

        LabelWidget labelWidget = new LabelWidget (scene);
        labelWidget.setLabel (atribute);
        widget.addChild (labelWidget);

        return widget;
    }

    public void addField (Widget fieldWidget) {
        fields.addChild (fieldWidget);
    }

    public void removeField (Widget fieldWidget) {
        fields.removeChild (fieldWidget);
    }

    public void addAction (Widget actionWidget) {
        actions.addChild (actionWidget);
    }

    public void removeAction (Widget actionWidget) {
        actions.removeChild (actionWidget);
    }

    public void addAtribute (Widget atributeWidget) {
        atributes.addChild (atributeWidget);
    }

    public void removeAtribute (Widget atributeWidget) {
        atributes.removeChild (atributeWidget);
    }
}
