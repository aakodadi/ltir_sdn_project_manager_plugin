/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.appfiletype;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.IOColorLines;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

@ActionID(
        category = "Build",
        id = "org.ltir.sdn.appfiletype.LoadMCodeAction"
)
@ActionRegistration(
        iconBase = "org/ltir/sdn/appfiletype/open_24.png",
        displayName = "#CTL_LoadMCodeAction"
)
@ActionReferences({
    @ActionReference(path = "Toolbars/Build", position = 343),
    @ActionReference(path = "Loaders/text/app+xml/Actions", position = 802, separatorAfter = 803)
})
@Messages("CTL_LoadMCodeAction=Load")
public final class LoadMCodeAction implements ActionListener {

    private final AppDataObject context;
    private final FileObject appXML;
    private final FileObject deviceXML;
    private final FileObject projectFolder;
    private final FileObject tmpFolder;
    private final FileObject netronomeCodeFolder;
    private final FileObject ezchipCodeFolder;
    private final String toolsDirectory = "/opt/ltiral";
    private final String netronomeLoaderDirectory = toolsDirectory + File.separator + "loaders/Netronome";
    private final String netronomeLoader = netronomeLoaderDirectory + File.separator + "load.sh";
    private final String ezchipLoaderDirectory = toolsDirectory + File.separator + "loaders/EZchip";
    private final String ezchipLoader = ezchipLoaderDirectory + File.separator + "run.sh";
    private final device currentDevice;
    private Color green = new Color(39, 174, 35);

    public LoadMCodeAction(AppDataObject context) throws IOException {
        this.context = context;
        appXML = context.getPrimaryFile();
        projectFolder = appXML.getParent();
        FileObject[] projectChilds = projectFolder.getChildren();

        FileObject tmpTestObj = null;
        for (FileObject projectChild : projectChilds) {
            if (projectChild.getName().equals("tmp")) {
                tmpTestObj = projectChild;
                break;
            }
        }
        tmpFolder = tmpTestObj;

        FileObject[] tmpChilds = tmpFolder.getChildren();
        FileObject netronomeCodeFolderTestObj = null;
        File netronomeCodeFolderTest = new File(tmpFolder.getPath() + File.separator + "netronome_code");
        if (netronomeCodeFolderTest.isDirectory()) {
            for (FileObject tmpChild : tmpChilds) {
                if (tmpChild.getName().equals("netronome_code")) {
                    netronomeCodeFolderTestObj = tmpChild;
                    break;
                }
            }
            netronomeCodeFolder = netronomeCodeFolderTestObj;
        } else {
            netronomeCodeFolder = tmpFolder.createFolder("netronome_code");
        }
        
        FileObject ezchipCodeFolderTestObj = null;
        File ezchipCodeFolderTest = new File(tmpFolder.getPath() + File.separator + "ezchip_code");
        if (ezchipCodeFolderTest.isDirectory()) {
            for (FileObject tmpChild : tmpChilds) {
                if (tmpChild.getName().equals("ezchip_code")) {
                    ezchipCodeFolderTestObj = tmpChild;
                    break;
                }
            }
            ezchipCodeFolder = ezchipCodeFolderTestObj;
        } else {
            ezchipCodeFolder = tmpFolder.createFolder("ezchip_code");
        }

        FileObject devTest = null;
        for (FileObject projectChild : projectChilds) {
            if (projectChild.getName().equals("device")) {
                devTest = projectChild;
                break;
            }
        }
        deviceXML = devTest;
        DocumentBuilder dBuilder;
        Document doc = null;
        try {
            dBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            doc = dBuilder.parse(new File(deviceXML.getPath()));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
        }

        Node deviceNode = doc.getElementsByTagName("Device").item(0);
        NamedNodeMap deviceAttrs = deviceNode.getAttributes();
        Node labelAttr = deviceAttrs.getNamedItem("Label");
        String label = labelAttr.getNodeValue();
        if (label.equals("Netronome")) {
            currentDevice = device.Netronome;
        } else {

            currentDevice = device.EZChip;
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        // TODO use context
        final InputOutput io = IOProvider.getDefault().getIO("Output - " + projectFolder.getName(), false);
        io.setFocusTaken(true);
        io.getOut().println("_________________________");
        io.getOut().println();
        io.getOut().println();
        io.getOut().println();
        io.getOut().println();
        String command = null;
        switch (currentDevice) {
            case Netronome:

                command = netronomeLoader;
                command += " ";
                command += netronomeCodeFolder.getPath() + File.separator + "datapath.uc";
                try {
                    IOColorLines.println(io, "Loading Code to Netronome...", green);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
                break;
            case EZChip:
                command = ezchipLoader;
                command += " ";
                command += ezchipCodeFolder.getPath() + File.separator;
                try {
                    IOColorLines.println(io, "Code generation for EZchip...", green);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
                break;
        }
        io.getOut().println(command);
        int exit_val = 0;
        final Process p;
        try {
            p = Runtime.getRuntime().exec(command);

            new Thread(new Runnable() {
                public void run() {
                    BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = null;

                    try {
                        while ((line = input.readLine()) != null) {
                            io.getOut().println(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            p.waitFor();
            exit_val = p.exitValue();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
        String sucess_msg = "Code successfully loaded to " + currentDevice.toString() + " platform.";
        String unknown_err_msg = "Error : Unexpected Error";
        if (exit_val == 0) {
            try {
                IOColorLines.println(io, sucess_msg, green);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        } else {
            io.getErr().println(unknown_err_msg);
        }
    }
}
