/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.appfiletype;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Messages({
    "LBL_App_LOADER=Files of App"
})
@MIMEResolver.NamespaceRegistration(
        displayName = "#LBL_App_LOADER",
        mimeType = "text/app+xml",
        elementNS = {"SDNApplication"}
)
@DataObject.Registration(
        mimeType = "text/app+xml",
        iconBase = "org/ltir/sdn/appfiletype/SampleSDNApplication.png",
        displayName = "#LBL_App_LOADER",
        position = 300
)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200
    ),
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300
    ),
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500
    ),
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600
    ),
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800
    ),
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
            position = 900,
            separatorAfter = 1000
    ),
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
            position = 1100,
            separatorAfter = 1200
    ),
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
            position = 1300
    ),
    @ActionReference(
            path = "Loaders/text/app+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400
    )
})
public class AppDataObject extends MultiDataObject {

    public AppDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        registerEditor("text/app+xml", true);
    }
/*
    @Override
    protected Node createNodeDelegate() {
        return new DataNode(this,
                Children.create(new AppChildFactory(this), true),
                getLookup());
    }
*/
    @Override
    protected int associateLookup() {
        return 1;
    }

    @MultiViewElement.Registration(
            displayName = "#LBL_App_EDITOR",
            iconBase = "org/ltir/sdn/appfiletype/SampleSDNApplication.png",
            mimeType = "text/app+xml",
            persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
            preferredID = "App",
            position = 1000
    )
    @Messages("LBL_App_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }

    DataFolder getParent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static class AppChildFactory extends ChildFactory<String> {

        private final AppDataObject aDObj;

        public AppChildFactory(AppDataObject aDObj) {
            this.aDObj = aDObj;
        }

        @Override
        protected boolean createKeys(List<String> list) {
            /*
            FileObject fObj = aDObj.getPrimaryFile();
            DocumentBuilder dBuilder;
            Document doc = null;
            try {
                dBuilder = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                doc = dBuilder.parse(new File(fObj.getPath()));
            } catch (ParserConfigurationException | SAXException | IOException ex) {
                return false;
            }

            NodeList nodeList = doc.getElementsByTagName("Table");
            
            for(int i=0; i<nodeList.getLength(); i++){
                org.w3c.dom.Node tableNode = nodeList.item(i);
                NamedNodeMap tableAtributes = tableNode.getAttributes();
                String tableName = tableAtributes.getNamedItem("Name").getNodeValue();
                list.add("Table[" + tableName + "]");
            }
                    */
            return true;
        }

        @Override
        protected Node createNodeForKey(String key) {
            Node childNode = new AbstractNode(Children.LEAF);
            childNode.setDisplayName(key);
            return childNode;
        }
    }

}
