/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn;

import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ProjectFactory;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author ltir-workstation-1
 */
@ServiceProvider(service = ProjectFactory.class)
public class SDNProjectFactory implements ProjectFactory {

    public static final String APPLICATION_FILE = "application.xml";

    //Specifies when a project is a project, i.e.,
    //if "application.xml" is present in a folder:
    @Override
    public boolean isProject(FileObject projectDirectory) {
        FileObject applicationFO = projectDirectory.getFileObject(APPLICATION_FILE);
        if (applicationFO == null) {
            return false;
        }
        DocumentBuilder dBuilder;
        Document doc = null;
        try {
            dBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            doc = dBuilder.parse(new File(applicationFO.getPath()));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            return false;
        }

        if (!doc.hasChildNodes()) {
            return false;
        }

        NodeList nodeList = doc.getElementsByTagName("Datapath");

        if (nodeList.getLength() == 0) {
            return false;
        }

        Node node = nodeList.item(0);

        if (!node.hasAttributes()) {
            return false;
        }

        NamedNodeMap nodeMap = node.getAttributes();
        node = nodeMap.getNamedItem("xmlns");
        if (node == null) {
            return false;
        }
        return node.getNodeValue().equals("SDNApplication");
    }

    //Specifies when the project will be opened, i.e., if the project exists:
    @Override
    public Project loadProject(FileObject dir, ProjectState state) throws IOException {
        return isProject(dir) ? new SDNProject(dir, state) : null;
    }

    @Override
    public void saveProject(final Project project) throws IOException, ClassCastException {
        // leave unimplemented for the moment
    }

}
