/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.panels;


import java.io.File;
import java.io.IOException;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.ltir.sdn.SDNProject;
import org.ltir.sdn.appfiletype.AppDataObject;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author ltir-workstation-1
 */
public class GeneralSDNProperties implements ProjectCustomizer.CompositeCategoryProvider {

    private static final String GENERAL = "General Properties";

    @ProjectCustomizer.CompositeCategoryProvider.Registration(
            projectType = "org-ltir-sdn", position = 10)
    public static GeneralSDNProperties createGeneral() {
        return new GeneralSDNProperties();
    }

    @NbBundle.Messages("LBL_Config_General=General")
    @Override
    public ProjectCustomizer.Category createCategory(Lookup lkp) {
        return ProjectCustomizer.Category.create(
                GENERAL,
                Bundle.LBL_Config_General(),
                null);
    }

    @Override
    public JComponent createComponent(ProjectCustomizer.Category category, Lookup lkp) {
        
        SDNProject project = lkp.lookup(SDNProject.class);
        assert project != null;
        String platform;
        String deviceXML = project.getProjectDirectory().getPath();
        String fileSeparator = System.getProperty("file.separator");
        deviceXML += fileSeparator + "device.xml";
        DocumentBuilder dBuilder;
        Document doc = null;
        try {
            dBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            doc = dBuilder.parse(new File(deviceXML));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
        }

        Node deviceNode = doc.getElementsByTagName("Device").item(0);
        NamedNodeMap deviceAttrs = deviceNode.getAttributes();
        Node labelAttr = deviceAttrs.getNamedItem("Label");
        String label = labelAttr.getNodeValue();
        if(label.equals("Netronome")){
            platform = "Netronome";
        } else {
            
            platform = "EZChip";
        }
        project.setPlatform(platform);
        JPanel panel = new JPanel();
        JPanel platformPanel = new JPanel();
        String[] platformes = {"Netronome", "EZChip"};
        platformPanel.add(new JLabel("Target Platform: "));
        JComboBox cb = new JComboBox(platformes);
        cb.setSelectedItem(platform);
        cb.setName("platformComboBox");
        platformPanel.add(cb);
        panel.add(platformPanel);
        return panel;
    }

}
