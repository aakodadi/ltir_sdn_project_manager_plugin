/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.nodes;

import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.ltir.sdn.SDNProject;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author ltir-workstation-1
 */
@NodeFactory.Registration(projectType = "org-netvirt-ltir-sdn", position = 10)
public class XMLNodeFactory implements NodeFactory {

    @Override
    public NodeList<?> createNodes(Project project) {
        SDNProject p = project.getLookup().lookup(SDNProject.class);
        assert p != null;
        return new TextsNodeList(p);
    }

    private class TextsNodeList implements NodeList<Node> {

        SDNProject project;

        public TextsNodeList(SDNProject project) {
            this.project = project;
        }

        @Override
        public List<Node> keys() {
            List<Node> result = new ArrayList<Node>();
            FileObject applicationFile = project.getProjectDirectory().getFileObject("application.xml");
            FileObject deviceFile = project.getProjectDirectory().getFileObject("device.xml");

            try {
                result.add(DataObject.find(applicationFile).getNodeDelegate());
                result.add(DataObject.find(deviceFile).getNodeDelegate());
            } catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            }
            
            return result;
        }

        @Override
        public Node node(Node node) {
            return new FilterNode(node);
        }

        @Override
        public void addNotify() {
        }

        @Override
        public void removeNotify() {
        }

        @Override
        public void addChangeListener(ChangeListener cl) {
        }

        @Override
        public void removeChangeListener(ChangeListener cl) {
        }
    }
}
