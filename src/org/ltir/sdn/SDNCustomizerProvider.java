/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.spi.project.ui.CustomizerProvider;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.util.Exceptions;
import org.openide.util.lookup.Lookups;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author ltir-workstation-1
 */
public class SDNCustomizerProvider implements CustomizerProvider {

    private JComboBox platformComboBox;
    public final SDNProject project;
    public static final String CUSTOMIZER_FOLDER_PATH
            = "Projects/org-ltir-sdn/Customizer";

    public SDNCustomizerProvider(SDNProject project) {
        this.project = project;
    }

    @Override
    public void showCustomizer() {
        Dialog dialog = ProjectCustomizer.createCustomizerDialog(
                //Path to layer folder:
                CUSTOMIZER_FOLDER_PATH,
                //Lookup, which must contain, at least, the Project:
                Lookups.fixed(project),
                //Preselected category:
                "",
                //OK button listener:
                new OKOptionListener(),
                //HelpCtx for Help button of dialog:
                null);
        dialog.setTitle("Project Properties - " + ProjectUtils.getInformation(project).getDisplayName());
        dialog.setVisible(true);

        getPlatformComboBox(((JLayeredPane) ((JRootPane) dialog.getComponent(0)).getComponent(1)).getComponent(0));
    }

    private void getPlatformComboBox(Component c) {
        if (c instanceof JPanel) {
            for (Component cmp : ((JPanel) c).getComponents()) {
                if (cmp instanceof JComboBox && cmp.getName().equals("platformComboBox")) {
                    platformComboBox = (JComboBox) cmp;
                    return;
                }
                getPlatformComboBox(cmp);
            }
        }
    }

    private class OKOptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String fileSeparator = System.getProperty("file.separator");
            int platform = platformComboBox.getSelectedIndex();
            String projectDirectory = project.getProjectDirectory().getPath() + fileSeparator;
            String netronomeXML = projectDirectory + "supported_platforms" + fileSeparator + "netronome.xml";
            String ezchipXML = projectDirectory + "supported_platforms" + fileSeparator + "ezchip.xml";
            String deviceXML = projectDirectory + "device.xml";
            String applicationXML = projectDirectory + "application.xml";
            DocumentBuilder dBuilder;
            Document doc = null;
            try {
                dBuilder = DocumentBuilderFactory.newInstance()
                        .newDocumentBuilder();
                doc = dBuilder.parse(new File(applicationXML));
            } catch (ParserConfigurationException | SAXException | IOException ex) {
            }

            NodeList fields = doc.getElementsByTagName("Field");
            NodeList metas = doc.getElementsByTagName("Meta");
            switch (platform) {
                case 0: {
                    try {
                        Files.copy(Paths.get(netronomeXML), Paths.get(deviceXML), REPLACE_EXISTING);
                    } catch (IOException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
                project.setPlatform("Netronome");
                for (int i = 0; i < fields.getLength(); i++) {
                    Element field = (Element) fields.item(i);
                    if (field.getAttribute("Name").equals("SMAC") || field.getAttribute("Name").equals("DMAC")) {
                        field.setAttribute("Size", "64");
                    } else if (field.getAttribute("Name").equals("VLAN_ID") || field.getAttribute("Name").equals("Ethertype") || field.getAttribute("Name").equals("L4_SP") || field.getAttribute("Name").equals("L4_DP") || field.getAttribute("Name").equals("VRF") || field.getAttribute("Name").equals("TUNNEL_ID") || field.getAttribute("Name").equals("INGRESS_PORT")) {
                        field.setAttribute("Size", "32");
                    }
                }
                for (int i = 0; i < metas.getLength(); i++) {
                    Element meta = (Element) metas.item(i);
                    if (meta.getAttribute("Name").equals("VRF") || meta.getAttribute("Name").equals("TUNNEL_ID") || meta.getAttribute("Name").equals("INGRESS_PORT")) {
                        meta.setAttribute("Size", "32");
                    }
                }
                break;
                case 1: {
                    try {
                        Files.copy(Paths.get(ezchipXML), Paths.get(deviceXML), REPLACE_EXISTING);
                    } catch (IOException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
                project.setPlatform("EZChip");
                for (int i = 0; i < fields.getLength(); i++) {
                    Element field = (Element) fields.item(i);
                    if (field.getAttribute("Name").equals("SMAC") || field.getAttribute("Name").equals("DMAC")) {
                        field.setAttribute("Size", "48");
                    } else if (field.getAttribute("Name").equals("VLAN_ID") || field.getAttribute("Name").equals("Ethertype") || field.getAttribute("Name").equals("L4_SP") || field.getAttribute("Name").equals("L4_DP")) {
                        field.setAttribute("Size", "16");
                    } else if (field.getAttribute("Name").equals("VRF") || field.getAttribute("Name").equals("TUNNEL_ID") || field.getAttribute("Name").equals("INGRESS_PORT")){
                        field.setAttribute("Size", "8");
                    }
                }
                for (int i = 0; i < metas.getLength(); i++) {
                    Element meta = (Element) metas.item(i);
                    if (meta.getAttribute("Name").equals("VRF") || meta.getAttribute("Name").equals("TUNNEL_ID") || meta.getAttribute("Name").equals("INGRESS_PORT")) {
                        meta.setAttribute("Size", "8");
                    }
                }
                break;
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = null;
            try {
                transformer = transformerFactory.newTransformer();
            } catch (TransformerConfigurationException ex) {
                Exceptions.printStackTrace(ex);
            }
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            DOMSource source = new DOMSource(doc);

            StreamResult result = new StreamResult(new File(applicationXML));
            try {
                transformer.transform(source, result);
            } catch (TransformerException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

}
