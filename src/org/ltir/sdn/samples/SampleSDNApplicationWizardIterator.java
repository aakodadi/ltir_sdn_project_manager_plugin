/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ltir.sdn.samples;

import java.awt.Component;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.templates.TemplateRegistration;
import org.netbeans.spi.project.ui.support.ProjectChooser;
import org.netbeans.spi.project.ui.templates.support.Templates;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbBundle.Messages;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

// TODO define position attribute
@TemplateRegistration(folder = "Project/Networking", displayName = "#SampleSDNApplication_displayName", description = "SampleSDNApplicationDescription.html", iconBase = "org/ltir/sdn/samples/SampleSDNApplication.png", content = "SampleSDNApplicationProject.zip")
@Messages("SampleSDNApplication_displayName=SDN Application")
public class SampleSDNApplicationWizardIterator implements WizardDescriptor./*Progress*/InstantiatingIterator {

    private int index;
    private WizardDescriptor.Panel[] panels;
    private WizardDescriptor wiz;

    public SampleSDNApplicationWizardIterator() {
    }

    public static SampleSDNApplicationWizardIterator createIterator() {
        return new SampleSDNApplicationWizardIterator();
    }

    private WizardDescriptor.Panel[] createPanels() {
        return new WizardDescriptor.Panel[]{
            new SampleSDNApplicationWizardPanel(),};
    }

    private String[] createSteps() {
        return new String[]{
            NbBundle.getMessage(SampleSDNApplicationWizardIterator.class, "LBL_CreateProjectStep")
        };
    }

    public Set/*<FileObject>*/ instantiate(/*ProgressHandle handle*/) throws IOException {
        Set<FileObject> resultSet = new LinkedHashSet<FileObject>();
        File dirF = FileUtil.normalizeFile((File) wiz.getProperty("projdir"));
        targetPlatform platform = (targetPlatform) wiz.getProperty("platform");
        dirF.mkdirs();

        FileObject template = Templates.getTemplate(wiz);
        FileObject dir = FileUtil.toFileObject(dirF);
        unZipFile(template.getInputStream(), dir);

        String fileSeparator = System.getProperty("file.separator");
        String projectDirectory = dirF.getAbsolutePath() + fileSeparator;
        String netronomeXML = projectDirectory + "supported_platforms" + fileSeparator + "netronome.xml";
        String ezchipXML = projectDirectory + "supported_platforms" + fileSeparator + "ezchip.xml";
        String deviceXML = projectDirectory + "device.xml";
        String applicationXML = projectDirectory + "application.xml";
        DocumentBuilder dBuilder;
        Document doc = null;
        try {
            dBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            doc = dBuilder.parse(new File(applicationXML));
        } catch (ParserConfigurationException | SAXException | IOException ex) {
        }

        NodeList fields = doc.getElementsByTagName("Field");
        NodeList metas = doc.getElementsByTagName("Meta");
        switch (platform) {
            case NETRONOME:
                try {
                    Files.copy(Paths.get(netronomeXML), Paths.get(deviceXML), REPLACE_EXISTING);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
                for (int i = 0; i < fields.getLength(); i++) {
                    Element field = (Element) fields.item(i);
                    if (field.getAttribute("Name").equals("SMAC") || field.getAttribute("Name").equals("DMAC")) {
                        field.setAttribute("Size", "64");
                    } else if (field.getAttribute("Name").equals("VLAN_ID") || field.getAttribute("Name").equals("Ethertype") || field.getAttribute("Name").equals("L4_SP") || field.getAttribute("Name").equals("L4_DP")|| field.getAttribute("Name").equals("VRF") || field.getAttribute("Name").equals("TUNNEL_ID") || field.getAttribute("Name").equals("INGRESS_PORT")) {
                        field.setAttribute("Size", "32");
                    }
                }
                for (int i = 0; i < metas.getLength(); i++) {
                    Element meta = (Element) metas.item(i);
                    if (meta.getAttribute("Name").equals("VRF") || meta.getAttribute("Name").equals("TUNNEL_ID") || meta.getAttribute("Name").equals("INGRESS_PORT")) {
                        meta.setAttribute("Size", "32");
                    }
                }
                break;
            case EZCHIP:
                try {
                    Files.copy(Paths.get(ezchipXML), Paths.get(deviceXML), REPLACE_EXISTING);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
                for (int i = 0; i < fields.getLength(); i++) {
                    Element field = (Element) fields.item(i);
                    if (field.getAttribute("Name").equals("SMAC") || field.getAttribute("Name").equals("DMAC")) {
                        field.setAttribute("Size", "48");
                    } else if (field.getAttribute("Name").equals("VLAN_ID") || field.getAttribute("Name").equals("Ethertype") || field.getAttribute("Name").equals("L4_SP") || field.getAttribute("Name").equals("L4_DP")) {
                        field.setAttribute("Size", "16");
                    } else if (field.getAttribute("Name").equals("VRF") || field.getAttribute("Name").equals("TUNNEL_ID") || field.getAttribute("Name").equals("INGRESS_PORT")){
                        field.setAttribute("Size", "8");
                    }
                }
                for (int i = 0; i < metas.getLength(); i++) {
                    Element meta = (Element) metas.item(i);
                    if (meta.getAttribute("Name").equals("VRF") || meta.getAttribute("Name").equals("TUNNEL_ID") || meta.getAttribute("Name").equals("INGRESS_PORT")) {
                        meta.setAttribute("Size", "8");
                    }
                }
                break;
        }
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException ex) {
            Exceptions.printStackTrace(ex);
        }
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        DOMSource source = new DOMSource(doc);

        StreamResult result = new StreamResult(new File(applicationXML));
        try {
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            Exceptions.printStackTrace(ex);
        }

        // Always open top dir as a project:
        resultSet.add(dir);
        // Look for nested projects to open as well:
        Enumeration<? extends FileObject> e = dir.getFolders(true);
        while (e.hasMoreElements()) {
            FileObject subfolder = e.nextElement();
            if (ProjectManager.getDefault().isProject(subfolder)) {
                resultSet.add(subfolder);
            }
        }

        File parent = dirF.getParentFile();
        if (parent != null && parent.exists()) {
            ProjectChooser.setProjectsFolder(parent);
        }

        return resultSet;
    }

    public void initialize(WizardDescriptor wiz) {
        this.wiz = wiz;
        index = 0;
        panels = createPanels();
        // Make sure list of steps is accurate.
        String[] steps = createSteps();
        for (int i = 0; i < panels.length; i++) {
            Component c = panels[i].getComponent();
            if (steps[i] == null) {
                // Default step name to component name of panel.
                // Mainly useful for getting the name of the target
                // chooser to appear in the list of steps.
                steps[i] = c.getName();
            }
            if (c instanceof JComponent) { // assume Swing components
                JComponent jc = (JComponent) c;
                // Step #.
                // TODO if using org.openide.dialogs >= 7.8, can use WizardDescriptor.PROP_*:
                jc.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
                // Step name (actually the whole list for reference).
                jc.putClientProperty("WizardPanel_contentData", steps);
            }
        }
    }

    public void uninitialize(WizardDescriptor wiz) {
        this.wiz.putProperty("projdir", null);
        this.wiz.putProperty("name", null);
        this.wiz = null;
        panels = null;
    }

    public String name() {
        return MessageFormat.format("{0} of {1}",
                new Object[]{new Integer(index + 1), new Integer(panels.length)});
    }

    public boolean hasNext() {
        return index < panels.length - 1;
    }

    public boolean hasPrevious() {
        return index > 0;
    }

    public void nextPanel() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        index++;
    }

    public void previousPanel() {
        if (!hasPrevious()) {
            throw new NoSuchElementException();
        }
        index--;
    }

    public WizardDescriptor.Panel current() {
        return panels[index];
    }

    // If nothing unusual changes in the middle of the wizard, simply:
    public final void addChangeListener(ChangeListener l) {
    }

    public final void removeChangeListener(ChangeListener l) {
    }

    private static void unZipFile(InputStream source, FileObject projectRoot) throws IOException {
        try {
            ZipInputStream str = new ZipInputStream(source);
            ZipEntry entry;
            while ((entry = str.getNextEntry()) != null) {
                if (entry.isDirectory()) {
                    FileUtil.createFolder(projectRoot, entry.getName());
                } else {
                    FileObject fo = FileUtil.createData(projectRoot, entry.getName());
                    if ("nbproject/project.xml".equals(entry.getName())) {
                        // Special handling for setting name of Ant-based projects; customize as needed:
                        filterProjectXML(fo, str, projectRoot.getName());
                    } else {
                        writeFile(str, fo);
                    }
                }
            }
        } finally {
            source.close();
        }
    }

    private static void writeFile(ZipInputStream str, FileObject fo) throws IOException {
        OutputStream out = fo.getOutputStream();
        try {
            FileUtil.copy(str, out);
        } finally {
            out.close();
        }
    }

    private static void filterProjectXML(FileObject fo, ZipInputStream str, String name) throws IOException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            FileUtil.copy(str, baos);
            Document doc = XMLUtil.parse(new InputSource(new ByteArrayInputStream(baos.toByteArray())), false, false, null, null);
            NodeList nl = doc.getDocumentElement().getElementsByTagName("name");
            if (nl != null) {
                for (int i = 0; i < nl.getLength(); i++) {
                    Element el = (Element) nl.item(i);
                    if (el.getParentNode() != null && "data".equals(el.getParentNode().getNodeName())) {
                        NodeList nl2 = el.getChildNodes();
                        if (nl2.getLength() > 0) {
                            nl2.item(0).setNodeValue(name);
                        }
                        break;
                    }
                }
            }
            OutputStream out = fo.getOutputStream();
            try {
                XMLUtil.write(doc, out, "UTF-8");
            } finally {
                out.close();
            }
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
            writeFile(str, fo);
        }

    }

}
